# README #

Created by Theodorus Yoga.

This repository contains a solution for Gojek Software Engineering Test.

This solution uses Java and Maven as the build automation tools. 

### Setting up the solution ###

Since this solution depends on Java and Maven, make sure that at least JDK 8 (http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) and Maven (https://maven.apache.org/install.html) is already installed on your system.
Please set configuration as follow in order to run the solution correctly:
1. Set environment variable "JAVA_HOME" (points to JDK installation)
2. Set environment variable "java" (points to binary file inside jdk/bin folder)
3.. Add bin directory from Maven installation to the PATH variable. Make sure that you are able to run "mvn -v" before proceeding. It ensures that you have successfully installed Maven.

## Running the solution ##

Point your terminal into the solution folder. 
Make sure that you have permission to execute the script file by running "chmod +x parking_lot.sh".
Run ./parking_lot to open interactive mode or run ./parking_lot [your text file] to execute commands from input file (txt).

### Others ###

Please inform me if the solution is corrupted/broken to this email: theoyoga@outlook.com.