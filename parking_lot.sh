echo "Checking Java installation..."
if ! type "java" > /dev/null; then
  echo "Java is not installed. Please install Java before running this solution."
  exit 1
  else 
  echo "OK"
fi
echo "Checking Maven installation..."
if ! type "mvn" > /dev/null; then
  echo "Maven is not installed. Please install Maven before running this solution."
  exit 1
  else 
  echo "OK"
fi
echo "Compiling..."
mvn install > out.log 2> /dev/null
echo "OK"
echo "Creating the package..."
mvn package > out.log 2> /dev/null
echo "OK"
echo "Running the solution..."
echo ""
if [ $# -eq 0 ]
  then
    java -jar target/gojek-test-app-1.0.jar
    else
    java -jar target/gojek-test-app-1.0.jar $1
fi