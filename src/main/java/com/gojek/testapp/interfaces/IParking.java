package com.gojek.testapp.interfaces;


public interface IParking{
    public void CreateNewParkingSlots(int slot);
    public int InsertParkingSlot(String registrationNumber, String colour);
    public Boolean LeaveParkingSlot(int slot);
    public String GetStatus();
    public String[] GetRegistrationNumbersFromColour(String colour);
    public String[] GetSlotsFromColour(String colour);
    public int GetSlotFromRegistrationNumber(String registrationNumber);
}