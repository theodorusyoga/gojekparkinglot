package com.gojek.testapp;

import com.gojek.testapp.helpers.Parser;

public class Process {
    private static Parking parking = new Parking();

    public static void ExecuteCreateParkingLot(String[] arguments){
        if(arguments.length < 2){
            System.out.println("You have not specified any slot number"); return;
        }

        int slot = Parser.parseInt(arguments[1]).orElse(0);

        if(slot == 0){
            System.out.println("Please enter slot value greater than 0"); return;
        }

        parking.CreateNewParkingSlots(slot);
        System.out.println("Created a parking lot with " + slot + " slots");
    }

    public static void ExecuteInsertCarToParkingLot(String[] arguments){
        if(arguments.length < 3){
            System.out.println("Command is not valid for parking the car"); return;
        }

        String registrationNumber = arguments[1];
        String colour = arguments[2];

        int slot = parking.InsertParkingSlot(registrationNumber, colour);

        if(slot == -2){
            System.out.println("Sorry, registration number cannot be the same");
        }
        else if(slot == -1){
            System.out.println("Sorry, parking lot is full");
        }
        else if(slot == 0){
            System.out.println("Parking lot has not created yet");
        }
        else{
            System.out.println("Allocated slot number: " + slot);
        }
    }

    public static void ExecuteLeaveParkingSlot(String[] arguments){
        if(arguments.length < 2){
            System.out.println("Please enter number of slot that you wish a car to leave"); return;
        }

        int slot = Parser.parseInt(arguments[1]).orElse(0);

        if(slot == 0){
            System.out.println("Please enter slot value greater than 0"); return;
        }

        boolean isLeaveSuccess = parking.LeaveParkingSlot(slot);
        if(isLeaveSuccess){
            System.out.println("Slot number " + slot + " is free"); 
        }
        else{
            System.out.println("Please enter valid slot number or the slot might be empty"); 
        }
    }

    public static void ExecuteGetStatus(){
        String status = parking.GetStatus();
        System.out.println(status);
    }

    public static void ExecuteGetNumberFromColour(String[] arguments){
        if(arguments.length < 2){
            System.out.println("Please enter valid colour"); return;
        }

        String colour = arguments[1];
        String[] numbers = parking.GetRegistrationNumbersFromColour(colour);

        if(numbers.length == 0){
            System.out.println("No car with the color registered"); return;
        }

        String commaSeparatedNumbers = String.join(", ", numbers);

        System.out.println(commaSeparatedNumbers);
    }

    public static void ExecuteGetSlotsFromColour(String[] arguments){
        if(arguments.length < 2){
            System.out.println("Please enter valid colour"); return;
        }

        String colour = arguments[1];
        String[] slots = parking.GetSlotsFromColour(colour);


        if(slots.length == 0){
            System.out.println("No car slot with the color registered"); return;
        }

        String commaSeparatedNumbers = String.join(", ", slots);

        System.out.println(commaSeparatedNumbers);
    }

    public static void ExecuteGetSlotFromRegistrationNumber(String[] arguments){
        if(arguments.length < 2){
            System.out.println("Please enter valid registration number"); return;
        }

        String registrationNumber = arguments[1];

        int slot = parking.GetSlotFromRegistrationNumber(registrationNumber);

        if(slot == -1){
            System.out.println("Not found");
        }
        else if(slot == 0){
            System.out.println("Parking lot has not created yet");
        }
        else{
            System.out.println(slot);
        }
    }
}