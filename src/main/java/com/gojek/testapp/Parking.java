package com.gojek.testapp;

import com.gojek.testapp.interfaces.IParking;
import com.gojek.testapp.helpers.Car;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Parking implements IParking {

	private List<Car> cars;

	public List<Car> GetCarSlots(){
		return this.cars;
	}

	public void CreateNewParkingSlots(int slot) {
		List<Car> cars = new ArrayList<Car>(slot);
		for (int i = 0; i < slot; i++) {
			cars.add(new Car());
		}
		this.cars = cars;
	}

	public int InsertParkingSlot(String registrationNumber, String colour) {
		if (this.cars == null || registrationNumber == "" || colour == "") {
			return 0;
		}

		try {
			Stream<Car> sameNumber = this.cars.stream().filter(p -> p.GetNumber().equals(registrationNumber.trim()));

			//if record exists
			if(sameNumber.count() > 0){
				return -2;
			}

			Optional<Car> emptySlot = this.cars.stream().filter(p -> p.GetNumber() == "" && p.GetColour() == "")
					.findFirst();

			int index = this.cars.indexOf(emptySlot.get());
			Car car = new Car(registrationNumber.trim(), colour);
			this.cars.set(index, car);
			return (index + 1);

		} catch (NullPointerException e) {
			return -1;
		} catch (NoSuchElementException e) {
			return -1;
		}
	}

	public Boolean LeaveParkingSlot(int slot) {
		if (this.cars == null) {
			return false;
		}

		try {
			if (CheckParkingSlotOccupied(slot)) {
				this.cars.set(slot - 1, new Car());
				return true;
			} else
				return false;

		} catch (UnsupportedOperationException e) {
			return false;
		} catch (IndexOutOfBoundsException e) {
			return false;
		}
	}

	public String GetStatus() {
		StringBuilder sb = new StringBuilder();
		sb.append("Slot No\tRegistration No.\tColour");

		//if the slot has not been initialised
		if (this.cars == null) {
			sb.append("\n----Parking slot is not available----");
			return sb.toString();
		}

		//if initialized, but size is 0
		if (this.cars.size() == 0) {
			sb.append("\n----No slot is available----");
			return sb.toString();
		}

		Stream<Car> emptySlot = this.cars.stream().filter(p -> p.GetNumber() == "" && p.GetColour() == "");

		//if slot is defined, but empty
		if (emptySlot.count() == this.cars.size()) {
			sb.append("\n----Parking slot is empty----");
			return sb.toString();
		}

		for (int i = 0; i < this.cars.size(); i++) {
			Car car = this.cars.get(i);
			if (car.GetNumber() != "" && car.GetColour() != "") {
				sb.append("\n");
				sb.append((i + 1) + "\t" + car.GetNumber() + "\t" + car.GetColour());
			}
		}

		return sb.toString();
	}

	public String[] GetRegistrationNumbersFromColour(String colour) {
		if (this.cars == null) {
			return new String[0];
		}

		Object[] cars = this.cars.stream().filter(p -> p.GetColour().equals(colour)).map(m -> m.GetNumber()).toArray();

		return Arrays.copyOf(cars, cars.length, String[].class);
	}

	public String[] GetSlotsFromColour(String colour) {
		List<String> slots = new ArrayList<String>();
		
		if (this.cars == null) {
			return new String[0];
		}

		List<Car> carsWithColour = this.cars.stream().filter(p -> p.GetColour().equals(colour))
												 .collect(Collectors.toList());

		for (Car car : carsWithColour) {
			int index = this.cars.indexOf(car);
			slots.add(String.valueOf(index+1));
		}

		String[] result = new String[carsWithColour.size()];
		slots.toArray(result);
		return result;
	}

	public int GetSlotFromRegistrationNumber(String registrationNumber) {
		if (this.cars == null) {
			return 0;
		}

		try {
			Optional<Car> car = this.cars.stream().filter(p -> p.GetNumber().equals(registrationNumber))
					.findFirst();
			int index = this.cars.indexOf(car.get());
			return (index + 1);

		} catch (NullPointerException e) {
			return -1;
		} catch (NoSuchElementException e) {
			return -1;
		}
	}

	//private methods
	private Boolean CheckParkingSlotOccupied(int slot) {
		Car car = this.cars.get(slot - 1);
		return car.GetColour() != "" && car.GetNumber() != "";
	}

}