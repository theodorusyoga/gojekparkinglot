package com.gojek.testapp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.stream.Stream;
import com.gojek.testapp.helpers.Command;


public class App {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        //start with entering the command
        if(args.length == 0){
            BeginCommand();
        }
        else {
            String file = args[0];
            if(file != ""){
                try(Stream<String> stream = Files.lines(Paths.get(file))){
                    stream.forEach(p -> DetermineCommand(p, true));
                    scanner.nextLine();
                }
                catch(IOException e){
                    System.out.println("File not found!");
                }
            }
            else{
                BeginCommand();
            }
        }
    }

    private static void BeginCommand(){
        System.out.print("Please enter your command: ");
        String command = scanner.nextLine();
        DetermineCommand(command, false);
    }

    private static void DetermineCommand(String argument, Boolean fromFile) {
        String[] arguments = argument.split(" ");

        //validate arguments
        if (arguments.length == 0) {
            System.out.println("Command is not valid");
            if(!fromFile) BeginCommand();
            return;
        }

        //determine command
        String command = arguments[0];

        //execute the process
        if(command.equals(Command.CREATE)){
            Process.ExecuteCreateParkingLot(arguments);
        } else if(command.equals(Command.PARK)){
           Process.ExecuteInsertCarToParkingLot(arguments);
        } else if(command.equals(Command.LEAVE)){
            Process.ExecuteLeaveParkingSlot(arguments);
        } else if(command.equals(Command.STATUS)){
            Process.ExecuteGetStatus();
        } else if(command.equals(Command.LISTNUMBERSWITHCOLOUR)){
            Process.ExecuteGetNumberFromColour(arguments);
        } else if(command.equals(Command.LISTSLOTSWITHCOLOUR)){
            Process.ExecuteGetSlotsFromColour(arguments);
        } else if(command.equals(Command.GETSLOTWITHNUMBER)){
            Process.ExecuteGetSlotFromRegistrationNumber(arguments);
        }
        else{
            System.out.println("Command is not valid");
        }
        
        //start from beginning
        if(!fromFile) BeginCommand();
    }
}
