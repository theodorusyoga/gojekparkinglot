package com.gojek.testapp.helpers;

public class Car{
    private String number;
    private String colour;

    public Car(){
        this.number = "";
        this.colour = "";
    }

    public Car(String number, String colour){
        this.number = number;
        this.colour = colour;
    }

    public void SetNumber(String number){
        this.number = number;
    }

    public String GetNumber(){
        return this.number;
    }

    public void SetColour(String colour){
        this.colour = colour;
    }

    public String GetColour(){
        return this.colour;
    }
}