package com.gojek.testapp.helpers;

public final class Command {
    public static String CREATE = "create_parking_lot";
    public static final String PARK = "park";
    public static final String LEAVE = "leave";
    public static final String STATUS = "status";
    public static final String LISTNUMBERSWITHCOLOUR = "registration_numbers_for_cars_with_colour";
    public static final String LISTSLOTSWITHCOLOUR = "slot_numbers_for_cars_with_colour";
    public static final String GETSLOTWITHNUMBER = "slot_number_for_registration_number";
}