package com.gojek.testapp.helpers;

import java.util.Optional;

public final class Parser{

    public static Optional<Integer> parseInt(String toParse) {
        try {
            return Optional.of(Integer.parseInt(toParse));
        } catch (NumberFormatException e) {
            return Optional.empty();
        } catch (NullPointerException e) {
            return Optional.empty();
        } 
    }
    
}
