package com.gojek.testapp;

import com.gojek.testapp.Parking;
import com.gojek.testapp.helpers.Car;

import junit.framework.TestCase;

public class ParkingTest extends TestCase {

    public static void testCreateParkingSlots() {
        Parking parking = new Parking();
        parking.CreateNewParkingSlots(6);

        //assert the same size of created slots
        assertEquals(6, parking.GetCarSlots().size());
    }

    public static void testInsertParkingSlots() {
        Parking parking = new Parking();
        parking.CreateNewParkingSlots(3);

        //insert correct record
        int slot1 = parking.InsertParkingSlot("KA-01-HH-1234", "White");
        assertEquals(1, slot1);
        Car car1 = parking.GetCarSlots().get(0);
        assertEquals("KA-01-HH-1234", car1.GetNumber());
        assertEquals("White", car1.GetColour());

        //insert same record
        int slot2 = parking.InsertParkingSlot("KA-01-HH-1234", "Red");
        assertEquals(-2, slot2);

        //insert empty record
        int slot3 = parking.InsertParkingSlot("", "");
        assertEquals(0, slot3);

        //insert record without number
        int slot4 = parking.InsertParkingSlot("", "Black");
        assertEquals(0, slot4);

        //insert record without colour
        int slot5 = parking.InsertParkingSlot("KA-01-HH-1234", "");
        assertEquals(0, slot5);

        //insert correct record
        int slot6 = parking.InsertParkingSlot("KA-01-HH-1236", "Brown");
        assertEquals(2, slot6);
        Car car6 = parking.GetCarSlots().get(1);
        assertEquals("KA-01-HH-1236", car6.GetNumber());
        assertEquals("Brown", car6.GetColour());
    }

    public static void testLeaveParkingSlot() {
        Parking parking = new Parking();
        parking.CreateNewParkingSlots(3);

        //insert records
        parking.InsertParkingSlot("KA-01-HH-1234", "White");
        parking.InsertParkingSlot("KA-01-HH-1236", "Brown");
        parking.InsertParkingSlot("KA-01-HH-1238", "Black");

        //leave
        Boolean result = parking.LeaveParkingSlot(2);
        assertTrue(result);
        Car carEmpty = parking.GetCarSlots().get(1);
        assertEquals("", carEmpty.GetColour());
        assertEquals("", carEmpty.GetNumber());
    }

    public static void testGetRegistrationNumbersByColour() {
        Parking parking = new Parking();
        parking.CreateNewParkingSlots(4);

        //insert records
        parking.InsertParkingSlot("KA-01-HH-1234", "White");
        parking.InsertParkingSlot("KA-01-HH-1236", "White");
        parking.InsertParkingSlot("KA-01-HH-1238", "Black");
        parking.InsertParkingSlot("KA-01-HH-1240", "Black");

        //get white
        String[] numbers1 = parking.GetRegistrationNumbersFromColour("White");
        assertEquals("KA-01-HH-1234", numbers1[0]);
        assertEquals("KA-01-HH-1236", numbers1[1]);

        //get black
        String[] numbers2 = parking.GetRegistrationNumbersFromColour("Black");
        assertEquals("KA-01-HH-1238", numbers2[0]);
        assertEquals("KA-01-HH-1240", numbers2[1]);
    }

    public static void testGetSlotsByColour() {
        Parking parking = new Parking();
        parking.CreateNewParkingSlots(4);

        //insert records
        parking.InsertParkingSlot("KA-01-HH-1234", "White");
        parking.InsertParkingSlot("KA-01-HH-1236", "White");
        parking.InsertParkingSlot("KA-01-HH-1238", "Black");
        parking.InsertParkingSlot("KA-01-HH-1240", "Black");

        //get white
        String[] slots1 = parking.GetSlotsFromColour("White");
        assertEquals("1", slots1[0]);
        assertEquals("2", slots1[1]);

        //get black
        String[] slots2 = parking.GetSlotsFromColour("Black");
        assertEquals("3", slots2[0]);
        assertEquals("4", slots2[1]);
    }

    public static void testGetSlotByRegistrationNumber() {
        Parking parking = new Parking();
        parking.CreateNewParkingSlots(4);

        //insert records
        parking.InsertParkingSlot("KA-01-HH-1234", "White");
        parking.InsertParkingSlot("KA-01-HH-1236", "White");
        parking.InsertParkingSlot("KA-01-HH-1238", "Black");
        parking.InsertParkingSlot("KA-01-HH-1240", "Black");

        //get slot
        int slot1 = parking.GetSlotFromRegistrationNumber("KA-01-HH-1234");
        assertEquals(1, slot1);
        int slot2 = parking.GetSlotFromRegistrationNumber("KA-01-HH-1236");
        assertEquals(2, slot2);
        int slot3 = parking.GetSlotFromRegistrationNumber("KA-01-HH-1238");
        assertEquals(3, slot3);
        int slot4 = parking.GetSlotFromRegistrationNumber("KA-01-HH-1240");
        assertEquals(4, slot4);
    }

}