package com.gojek.testapp;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;;

public class AppTest 
    extends TestCase
{
    
    public AppTest( String testName )
    {
        super( testName );
    }

    public static Test suite()
    {
        return new TestSuite( ParkingTest.class ); //test the suite for ParkingTest class
    }
}
